Attendance Managament System:

App name:- polls

>>>To view all students present today:
    polls/present/all
    eg:  http://127.0.0.1:8000/polls/present/all/


>>>To view all students present in a given class:
    polls/present/class<X>
    eg:  http://127.0.0.1:8000/polls/present/class3


>>>To view all students absent today:
    polls/absent/all
    eg:  http://127.0.0.1:8000/polls/absent/all/


>>>To view all students absent in a given class:
    polls/absent/class<X>
    eg:  http://127.0.0.1:8000/polls/absent/class3


>>>To check status of a particlar student:
    polls/check/<name>
    eg: http://127.0.0.1:8000/polls/check/Akshay


>>>To get Emergency Contact of a particlar student:
    polls/contact/<name>
    eg: http://127.0.0.1:8000/polls/contact/Akshay
