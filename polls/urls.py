from django.views.decorators.csrf import csrf_exempt
from django.conf.urls import url
from .views import *
#from views import TestProject1
urlpatterns = [
    url(r'^present/class(?P<classd>.*)$', students.as_view(), name = 'students'),
    url(r'^present/all/$', ptoday.as_view(), name='ptoday'),
    url(r'^check/(?P<named>.*)$', check.as_view(), name='check'),
    url(r'^contact/(?P<phoned>.*)$', contact.as_view(), name='contact'),
    url(r'^absent/class(?P<classd>.*)$', absent.as_view(), name = 'absent'),
    url(r'^absent/all/$', atoday.as_view(), name='atoday')
]
