from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse
from models import Person


#To view all students absent in a given class:

class students(View):
    def get(self, request, classd):
        _params = request.GET
        outt= classd
        out8 = int(classd)
        if out8 <= 10:
            classd = Person.objects.filter(classx = classd , status = "P")
            output = ' , '.join(q.name for q in classd)
            sub = ", "
            count  = output.count(sub)
            out2 = count + 1
            out3 = str(out2)
            out4 = out3 + '  > > >  '
            return HttpResponse("Students Present in Class" +outt+ "  are  " + out4  + output)
        else:
            return HttpResponse("ERROR!!! Classes are from 1st to 10th")


#To view all students absent today:

class ptoday(View):
    def get(self,request):
        _params = request.GET
        name = Person.objects.filter(status = "P")
        out = ' , '.join(x.name for x in name )
        sub = ", "
        count  = out.count(sub)
        out2 = count + 1
        out3 = str(out2)
        out5 = out3 + '  > > >  '
        return HttpResponse(" All Students Present Today are  "  + out5 + out)


#To check status of a particlar student:

class check(View):
    def get(self,request, named):
        _params = request.GET
        outt= named
        names = Person.objects.filter(name = named)
        output = ' , '.join(q.name for q in names)
        if not output:
            return HttpResponse(outt + " is not available in DataBase")
        else:
            names = Person.objects.filter(name = named, status = "P")
            output = ' , '.join(q.name for q in names)
            if not output:
                return HttpResponse(outt + " is Absent")
            else:
                return HttpResponse(outt + " is Present")


#To get Emergency Contact of a particlar student:

class contact(View):
    def get(self,request, phoned):
        _params = request.GET
        outt= phoned
        names = Person.objects.filter(name = phoned)
        output = ' '.join(q.phone for q in names)
        if not output:
            return HttpResponse(outt + " is not available in DataBase")
        else:
            return HttpResponse(" Emergency Contact Number of " + outt + " is :- " + output)


#To view all students absent in a given class

class absent(View):
    def get(self, request, classd):
        _params = request.GET
        outt= classd
        out8 = int(classd)
        if out8 <= 10:
            classd = Person.objects.filter(classx = classd , status = "A")
            output = ' , '.join(q.name for q in classd)
            if not output:
                return HttpResponse(" Everyone is Present in class" +outt)
            else:
                sub = ", "
                count  = output.count(sub)
                out2 = count + 1
                out3 = str(out2)
                out4 = out3 + '  > > >  '
                return HttpResponse("Student Absent in Class" +outt+ "  is  " + out4  + output)
        else:
            return HttpResponse("ERROR!!! Classes are from 1st to 10th")


#To view all students absent today:

class atoday(View):
    def get(self,request):
        _params = request.GET
        name = Person.objects.filter(status = "A")
        out = ' , '.join(x.name for x in name )
        sub = ", "
        count  = out.count(sub)
        out2 = count + 1
        out3 = str(out2)
        out5 = out3 + '  > > >  '
        return HttpResponse(" All Students Absent Today are  "  + out5 + out)
