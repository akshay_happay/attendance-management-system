# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models



class Person(models.Model):
    name = models.CharField(max_length=30)
    classx = models.CharField(max_length=2)
    dob = models.DateField()
    phone = models.CharField(max_length=10,default = "100")
    status = models.CharField(max_length=1,default = "P")

    def __str__(self):
        return self.name
